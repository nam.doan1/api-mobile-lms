
var ini = require('ini');
var fs = require('fs');
var conn = require('../connection');
async function Content_Curriculum_Read(curriculum) {
    return ini.parse(fs.readFileSync('./../lab/content/curricula/' + curriculum + '/info.dat', 'utf-8'));
}
function Array_Fields_JSONParse(array, fields) {
    keys = Object.keys(array);
    var arrayResult = {};
    keys.forEach(key => {
        if (fields.includes(key)) arrayResult[key] = JSON.parse(array[key]);
    });
    return arrayResult;
}
function Array_Items_JSONParse(array, fields) {
    var arrayResult = [];
    for (let key in array) {
        let item = array[key];
        arrayResult.push(Array_Fields_JSONParse(item, fields));
    }
    return arrayResult;
}
function Array_Integrate(dest, dest_field, source, source_field, store) {

    dest.forEach(dest_row => {
        let search = dest_row[dest_field];
        search.every(source_row => {
            if (source_row[source_field] == search) {
                dest_row[store] = source_row;
                return false;
            }
            return true;
        });
    });
    return dest;
}
function Array_Integrate_Direct(dest, field, data, store = false) {
    dest.forEach(row => {
        let id = row[field];
        let source = data[id];
        if (store) row[store] = source;
        else {
            for (let key in source) {
                let value = source[key];
                row[key] = value;
            }
        }
    });
    return dest;
}
function array_unique(array) {
    var flags = [], output = [], l = array.length, i;
    for (i = 0; i < l; i++) {
        if (flags[array[i]]) continue;
        flags[array[i]] = true;
        output.push(array[i].age);
    }
    return output;
}
function Project_Read(id, files = true) {
    project = ini.parse(fs.readFileSync('./../lab/content/projects/' + id + '/info.dat', 'utf-8'));
    if (files) {
        project["files"] = '';
    }
    return project;
}
function Projects_Read(ids, files = false) {
    data = [];

    ids = array_unique(ids);
    ids.forEach(id => {
        data[id] = Project_Read(id, files);
    });
    return data;
}
function array_column(array, column) {
    return array.map(item => item[column]);
}
async function Projects_List_ByStudent(student_id = -1, options = []) {
    if (student_id == -1) return {};

    var data = await new Promise((resolve, reject) => {
        conn.query('SELECT * FROM projects_students WHERE student_id = ' + student_id + ' ORDER BY -date_due', function (err, results, fields) {
            if(err) console.log(err)
            resolve(results);
        })
    })
    data = Array_Items_JSONParse(data, ["assessment"]);

    // OPTIONAL PROJECT DATA
    if (options["info"]) {
        // REPLACE PROJECT_ID (DATABASE ROW) WITH PROJECT_ID (SOURCE)	
        let ids = array_column(data, "project_id");
        let list = '(' + ids.toString() + ')';
        let query = "SELECT id, project_id FROM projects WHERE id IN " + list;
        let info = new Promise((resolve, reject) => {
            conn.query(query, function (err, results, fields) {
                resolve(results);
            })
        })
        data = Array_Integrate(data, "project_id", info, "id");

        ids = array_column(data, "project_id");
        let info2 = Projects_Read(ids);
        data = Array_Integrate_Direct(data, "project_id", info2, "data");
    }

    return data;
}

async function Progress_Curriculum_Data(student_id = -1, curriculum = "default", classes = 30) {
    data = [];

    // STUDENT
    if (student_id == -1) { return {}; }

    // CURRENT DATE
    let today = new Date();
    let yyyy = today.getFullYear();
    let mm = today.getMonth() + 1; // Months start at 0!
    let dd = today.getDate();
    let hh = today.getHours();
    let ii = today.getMinutes();
    if (dd < 10) dd = '0' + dd;
    if (mm < 10) mm = '0' + mm;
    if (hh < 10) hh = '0' + hh;
    if (ii < 10) ii = '0' + ii;
    var now = yyyy + '' + mm + '' + dd + '' + hh + '' + ii;
    // READ CURRICULUM, ITS SKILLS, AND EXTRACT THE ASSOCIATED PROGRAMS
    data["curriculum"] = await Content_Curriculum_Read(curriculum);
    let programs = data["curriculum"]["main"]["programs"];

    data["skills"] = [];
    for (let category of ["core skills", "extra skills"]) {
        let ids = Object.keys(data["curriculum"][category] || {});
        if (typeof data["skills"][category] == 'undefined') data["skills"][category] = {};
        ids.forEach(id => {
            data["skills"][category][id] = ini.parse(fs.readFileSync("./../lab/content/skills/" + id + '/info.dat', 'utf-8'));
        });
    }

    // CREATE FILTER TO ISOLATE CLASSES PERTAINING THIS SPECIFIC CURRICULUM
    programs = programs.split(",");
    let filter = [];
    programs.forEach(program => {
        filter.push("(classes.lesson_id LIKE '" + program + "%')")
    });

    filter = filter.join(' OR ');


    // ASSEMBLE QUERY TO EXTRACT CLASSES FOR THIS CURRICULUM
    let query = "SELECT classes_seats.id, classes.lesson_id, classes.date_start, classes_seats.assessment FROM  classes, classes_seats"
        + " WHERE(classes_seats.student_id = " + student_id + ") AND(classes_seats.class_id = classes.id) AND(classes.date_start < " + now + ")"
        + " AND(" + filter + ") ORDER BY classes.date_start  LIMIT 0, " + classes;


    // GET CLASSES
    var classes = await new Promise(resolve => {
        conn.query(query, function (err, results, fields) {
            resolve(results);
        });
    });
    for (let key in classes) {
        let oneclass = classes[key];
        oneclass["assessment"] = JSON.parse(oneclass["assessment"]);
    }
    data["classes"] = classes;


    // GET PROJECTS
    projects = await Projects_List_ByStudent(student_id);
    data["projects"] = projects;

    // RETURN
    return data;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function Object_Values_Average(obj) {
    if (!obj) return 0;
    var values = Object.values(obj);
    if (values.length == 0) return undefined;

    var sum = values.reduce((a, b) => parseInt(a) + parseInt(b), 0);
    var avg = (sum / values.length) || 0;

    return avg;
}
function Progress_Compare_Skillset(set_a, set_b) {
    distances = [];

    for (var skill in set_a) {
        var d = Math.abs(set_a[skill] - set_b[skill]);
        if (isNaN(d)) var d = 5;

        distances.push(d);
    }

    var similarity = (5 - Object_Values_Average(distances)) / 5;

    return similarity;
}
function Assessment_Report_Grade(assessment, fields = false) {
    if (!fields) var values = assessment;
    else {
        var values = {};
        for (let field of fields) values[field] = assessment[field];
    }
    var average = Object_Values_Average(values);

    return average;
}
function Object_Subset(obj, tag, mode = "starts-with") {
    var sub = {};
    var keys = Object.keys(obj);

    switch (mode) {
        case "starts-with":
            for (var key of keys) {
                if (key.toLowerCase().startsWith(tag.toLowerCase())) {
                    sub[key] = obj[key];
                }
            }
            break;

        case "starts-without":
            for (var key of keys) {
                if (!key.toLowerCase().startsWith(tag.toLowerCase())) {
                    sub[key] = obj[key];
                }
            }
            break;

        case "collect":
            {
                for (var key of keys) {
                    if (tag.includes(key)) {
                        sub[key] = obj[key];
                    }
                }
            }
    }

    return sub;
}

async function getProgress(req, res) {
    var student_id = req.body.student_id;
    var result = {};
    result["overall"] = [];
    result["core-skills"] = {};

    var data = await Progress_Curriculum_Data(student_id);
    data["core-skills"] = Object.keys(data["skills"]["core skills"]);
    for (let key in data["classes"]) {
        let value = data["classes"][key];
        data["classes"][key]["overall"] = Assessment_Report_Grade(value["assessment"]);
        data["classes"][key]["core skills"] = Assessment_Report_Grade(value["assessment"], data["core-skills"]);
        // reponsive data overrall
        result["overall"].push(value["overall"])
    }
    data["extra-skills"] = Object.keys(data["skills"]["extra skills"]);
    data["extraskills-averages"] = {};
    for (var skill of data["extra-skills"]) {
        // COLLECT VALUES AND AVERAGE
        var score = 0;
        var c = 0;
        for (var item of data["projects"]) {
            var value = item["assessment"][skill];

            if (value) {
                score = score + parseInt(value);
                c++;
            }
        }

        if (c == 0) var score = 0; else score = score / c;

        data["extraskills-averages"][skill] = score;
    }
    
    // reponsive data core skills
    for (var skill of data["core-skills"]) {
        // COLLECT VALUES
        var set = [];
        for (var item of data["classes"]) {
            var score = item["assessment"][skill];
            set.push(score);
        }

        result["core-skills"][skill] = set;
    }
    // reponsive data extra skills
    result["extra-skills"] = data["extraskills-averages"];

    // matching profile
    var profiles = Object_Subset(data["curriculum"], "profile");
    var max = 0;
    var similar = "none";
    for (var profile in profiles) {
        var similarity = Progress_Compare_Skillset(profiles[profile], data["extraskills-averages"]);

        if (similarity > max) {
            max = similarity;
            similar = profile;
        }
    }

    similarity = max;
    // reponsive data ORIENTATION
    result["orientation"] = data["curriculum"][similar];
    //reponsive profile
    similarity = Math.floor(100 * similarity);
    if (typeof result["profile"] == "undefined") result["profile"] = {};
    result["profile"]["name"] = (data["curriculum"]["profile researcher"]["en"]).toUpperCase() + "(" + similarity + "%)";
    result["profile"]["picture"] = "content/curricula/default/researcher.jpg";

    // reponsive outcome
    string = '{"A1":{"Flowery":{"Floral":{"Coffee Blossom":{},"Tea Rose":{}},"Fragrant":{"Cardamon Caraway":{},"Coriander Seeds":{}}},"Fruity":{"Citrus":{"Lemon":{},"Apple":{}},"Berry-like":{"Apricot":{},"Blackberry":{}}},"Herby":{"Alliaceous":{"Onion":{},"Garlic":{}},"Leguminous":{"Cucumber":{},"Garden Peas":{}}}},"A2":{"Resinous":{"Turpeny":{"Piney":{},"Blackcurrant-like":{}},"Medicinal":{"Camphoric":{},"Cineolic":{}}},"Spicy":{"Warming":{"Cedar":{},"Pepper":{}},"Pungent":{"Clove":{},"Thyme":{}}},"Carbony":{"Smokey":{"Tarry":{},"Pipe Tobacco":{}},"Ashy":{"Burnt":{},"Charred":{}}}},"B1":{"Nutty":{"Nut-like":{"Roasted Peanuts":{},"Walnuts":{}},"Malt-like":{"Balsamic Rice":{},"Toast":{}}},"Carmelly":{"Candy-like":{"Roasted Hazelnut":{},"Roasted Almond":{}},"Syrup-like":{"Honey":{},"Maple Syrup":{}}},"Chocolatey":{"Chocolate-like":{"Bakers":{},"Dark Chocolate":{}},"Vanilla-like":{"Swiss":{},"Butter":{}}}},"B2":{"Flowery":{"Floral":{"Coffee Blossom":{},"Tea Rose":{}},"Fragrant":{"Cardamon Caraway":{},"Coriander Seeds":{}}},"Fruity":{"Citrus":{"Lemon":{},"Apple":{}},"Berry-like":{"Apricot":{},"Blackberry":{}}},"Herby":{"Alliaceous":{"Onion":{},"Garlic":{}},"Leguminous":{"Cucumber":{},"Garden Peas":{}}}},"C1":{"Resinous":{"Turpeny":{"Piney":{},"Blackcurrant-like":{}},"Medicinal":{"Camphoric":{},"Cineolic":{}}},"Spicy":{"Warming":{"Cedar":{},"Pepper":{}},"Pungent":{"Clove":{},"Thyme":{}}},"Carbony":{"Smokey":{"Tarry":{},"Pipe Tobacco":{}},"Ashy":{"Burnt":{},"Charred":{}}}}}';
    result["outcome"] = JSON.parse(string);
    res.send(result);
}
module.exports = { getProgress };