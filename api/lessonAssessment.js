function String_Capitalize_Initial(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function String_Filter_AllowDigits(string, allowspaces) {
    if (allowspaces) {
        return string.replace(/[^0-9 ]/gi, '');
    }
    else {
        return string.replace(/[^0-9]/gi, '');
    }
}
function Object_Values_Average(obj) {
    var values = Object.values(obj);
    if (values.length == 0) return undefined;

    var sum = values.reduce((a, b) => parseInt(a) + parseInt(b), 0);
    var avg = (sum / values.length) || 0;

    return avg;
}
function Assessment_Report_Grade(assessment, fields = false) {
    if (!fields) var values = assessment;
    else {
        var values = {};
        for (var field of fields) values[field] = assessment[field];
    }

    var average = Object_Values_Average(values);

    return average;
}
function Assessment_Category_Transcribe(category, assessment, threshold = 3) {
    var parts = { good: [], bad: [] };

    // SEPARATE GOOD FROM BAD OUTCOMES ACCORDING TO THRESHOLD
    for (var outcome_id in category) {
        for (var assessment_id in assessment) {
            if (assessment_id == outcome_id) {
                var value = assessment[assessment_id];
                var info = category[assessment_id]["lv " + value];
                var text = info["en"];

                if (text) {
                    if (value < threshold) parts["bad"].push(text); else parts["good"].push(text);
                }

            }
        }
    }


    // DECODE
    var paragraphs = [];
    var person = "You";


    // PUT TOGETHER THE GOOD FIRST, THEN THE BAD. ADD A "," BETWEEN PARTS, "AND" ON THE LAST
    for (var category of ["good", "bad"]) {
        var text = "";
        var tokens = parts[category];

        for (var i = 0; i < tokens.length; i++) {
            text = text + tokens[i];

            if (i == tokens.length - 2) {
                text = text + " and ";
            }
            else
                if (i < tokens.length - 1) {
                    text = text + ", ";
                }
        }


        // IF AT LEAST ONE LINE OF TEXT, ADD PARAGRAPH
        if (text != "") {
            paragraphs.push(
                {
                    category: category,
                    text: person + " " + text
                });
        }

    }


    // ASSEMBLE GOOD AND BAD 
    switch (paragraphs.length) {
        case 0:
            var text = "";
            break;

        case 1:
            // ONLY GOOD OR ONLY BAD
            var text = String_Capitalize_Initial(paragraphs[0]["text"]);
            break;

        default:
            // GOOD AND BAD
            var however = "However";
            var text = String_Capitalize_Initial(paragraphs[0]["text"]) + ". " + String_Capitalize_Initial(however) + ", " + paragraphs[1]["text"];
            break;
    }

    return text;
}
function Assessment_Report_Transcribe(outcomes, assessment, more = {}, threshold = 3) {
    var paragraphs = [];

    // BEHAVIOR DATA, IF AVAILABLE
    var parts = [];

    if (more["behavior"]) {
        var behavior = more["behavior"];
        var fakeBehaviorData = {
            "lv 1": { "en": "Uncooperative and uninterested" },
            "lv 2": { "en": "Distracted" },
            "lv 3": { "en": "Good" },
            "lv 4": { "en": "Attentive" },
            "lv 4": { "en": "Cooperative and engaged" }
        }
        var text = fakeBehaviorData[behavior]["en"];
        text = text.toLowerCase();
        text = "Your class behavior was " + text;

        parts.push(text);
    }

    if (more["attendance"]) {
        var attendance = more["attendance"];
        var fakeAttendanceData = {
            "yes": { "en": "Attended the class on time" },
            "late": { "en": "Arrived late" },
            "miss": { "en": "Missed the class" },
        }
        var text = fakeAttendanceData[attendance]["en"];
        text = text.toLowerCase();
        text = "You " + text;

        // IF THE CLASS WAS MISSED, STOP HERE. JUST RETURN THE "MISSED CLASS" TEXT.
        if (attendance == "miss") return text;

        parts.push(text);
    }

    // BEHAVIOR OR ATTENDANCE ONLY
    if (parts.length == 1) {
        paragraphs.push(parts[0]);
    }
    else
        // ATTENDANCE AND BEHAVIOR
        if (parts.length == 2) {
            var timely = (attendance == "yes");
            var attentive = (parseInt(String_Filter_AllowDigits(behavior)) >= 3);
            // TIMELY AND ATTENTIVE
            if (timely && attentive) var connector = "and";

            // TIMELY BUT NOT ATTENTIVE
            if (timely && !attentive) var connector = "but";

            // LATE BUT ATTENTIVE
            if (!timely && attentive) var connector = "however";

            // LATE AND NOT ATTENTIVE
            if (!timely && !attentive) var connector = "and";

            var text = parts[1] + ", " + connector + " " + parts[0].toLowerCase();
            paragraphs.push(text);
        }



    // ONE PARAGRAPH PER CATEGORY (OUTCOMES, CORE SKILLS, EXTRA SKILLS, ETC. ...)
    for (var id in outcomes) {
        var category = outcomes[id];

        var text = Assessment_Category_Transcribe(category, assessment, threshold)

        if (text.trim()) paragraphs.push(text);
    }



    // EXPLICIT FEEDBACK
    if (more["feedback"]) {
        var text = more["feedback"].trim();
        text = String_Capitalize_Initial(text);

        if (text) paragraphs.push(text);
    }



    // OVERALL GRADING
    var grade = Assessment_Report_Grade(assessment);
    grade = Math.floor(grade);

    if (grade < 1 || isNaN(grade)) {
        // NOT GRADED YET
    }
    else {
        var fakeGradeText = ["Poor", "Mediocre", "Average", "Good", "Great!"];
        grade = fakeGradeText[grade - 1]
        var text = String_Capitalize_Initial("your overall grade") + ": " + grade.toUpperCase();

        paragraphs.push(text);
    }

    // FINAL CLEANUP
    text = text.trim();

    if (text == "") return "Hold on. Your performance for this Lesson is still being assessed. Please check again later.";

    return paragraphs;
}
function Course_Class_AssessmentFeedback(data, textonly) {

    // GET ALL ASSESSMENT DATA FOR THIS STUDENT FOR THIS LESSON
    var assessment = data["seat"]["assessment"];

    // GET ALL OUTCOMES FOR THIS LESSON, CATEGORIZED
    var outcomes = [];
    for (var category of ["outcomes", "core skills", "extra skills"]) {
        outcomes[category] = data["lesson"][category];
    }

    // BEHAVIOR, ATTENDANCE AND FEEDBACK TAKEN DIRECTLY FROM SEAT
    var more = data["seat"];

    // TRANSCRIBE 
    var text = Assessment_Report_Transcribe(outcomes, assessment, more) ;
    if (textonly) return text;

    return text;
}
function Assessment_Display_Assessment(category, outcomes, assessment) {
    data = {};
    for (let key in outcomes) {;
        let value = outcomes[key];
        data[String_Capitalize_Initial(value["info"]["en"])] = assessment[key] || null;
    }
   
    return data;
}
module.exports = (data) => {
    var assessment = data["seat"]["assessment"];
    var assessmentResult = {}
   
    // TEACHER FEEDBACK
    assessmentResult["teacherfeedback"] = Course_Class_AssessmentFeedback(data);
    // OUTCOMES FOR LESSON AIMS AND SKILLS
    for (var category of ["outcomes", "core skills"]) {
        let outcomes = data["lesson"][category];
        if (category == "core skills")
            assessmentResult["coreskills"] = Assessment_Display_Assessment(category, outcomes, assessment);
        else assessmentResult[category] = Assessment_Display_Assessment(category, outcomes, assessment);
    }
    return assessmentResult;
}