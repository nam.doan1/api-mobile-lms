
var rawurlencode = require('rawurlencode');
var fs = require('fs');
function Array_Items_Sort(array, field) {
    array.sort(
        function (a, b) {
            if (a[field] > b[field]) return 1;
            else
                if (a[field] < b[field]) return -1;
                else
                    return 0;
        });
}
function Assessment_Report_ChartData(items) {
    var chartdata = [0, 0, 0];

    items.forEach(item => {
        if (item["score"] <= 0.25) chartdata[2] += 1;
        else if (item["score"] <= 0.5) chartdata[1] += 1;
        else chartdata[0] += 1;
    });

    return chartdata;
}

function Course_Class_RowChart(results, state) {
    switch (state) {
        case "pre":
            // SORT BY SCORE TO GET THE FIRST
            Array_Items_Sort(results, "date");
            var index = 0;
            break;

        case "post":
            // SORT BY SCORE TO GET THE BEST
            Array_Items_Sort(results, "score");
            var index = results.length - 1;
            break;
    }

    var items = (typeof results[index] != "undefined")?results[index]["data"] || [] : [];
    var chartdata = Assessment_Report_ChartData(items);

    if (chartdata.reduce((a, b) => a + b, 0) != 0) return chartdata;
    else return {};
}
function Numbers_PercentageDistance(a, b) {
    return (b - a) * 100;
}
function Assessment_Grade_Display(score) {
    var fakeGradeText = ["Poor", "Mediocre", "Average", "Good", "Great!"];
    let index = Math.floor(score * 5) + Math.floor(score * 10) % 2;
    return fakeGradeText[index - 1];
}
function Course_Class_RowChartText(results, state) {
    // ANALYZE RESULTS
    if (results && results.length > 0) {
        Array_Items_Sort(results, "date");
        var first = results[0];

        Array_Items_Sort(results, "score");
        var best = results[results.length - 1];

        if (results.length > 1) {
            var improvement = Math.floor(Numbers_PercentageDistance(first["score"], best["score"]));
        }
    }


    // CHART TEXT
    if (!results) {
        var text = "";
        var subtext = "";
    }
    else
        if (results.length == 0) {
            var text = "N/A"
            var subtext = "No Result Yet";
        }
        else
            switch (state) {
                case "pre":
                    if (results.length > 0) {
                        var text = Assessment_Grade_Display(first["score"]);
                        var subtext = "";
                    }
                    else {
                        //var text =
                    }
                    break;

                case "post":
                    if (results.length == 1) {
                        var text = Assessment_Grade_Display(first["score"]);
                        var subtext = "Test Yourself Again";
                    }
                    else {
                        var text = Assessment_Grade_Display(best["score"]);
                        var subtext = "You Improved " + improvement + "%";
                    }
                    break;
            }


    return [text, subtext];
}
/////////////////////////////////////////////////////////////////////////////////////////ACTIVITIES//////////////////////////////
function Course_Class_ActivityVideo(data, state, source) {
    var dataresult = {};
    // COVER IMG
    dataresult["img"] = "content/lessons/" + data["lesson"]["source"] + "/" + rawurlencode(source) + "cover.png";


    // COVER CAPTION
    dataresult["caption"] = "VIDEO";

    //VIDEO
    data["content"].every(contentName => {
        if (contentName.search('video') >= 0) { videoFolder = contentName; return false; }
        return true;
    });
    if (typeof videoFolder != "undefined")
        videoLink = "content/lessons/" + data["lesson"]["source"] + "/" + rawurlencode(videoFolder) + "/video.mp4";
    else videoLink = "";
    // ACTIONS
    dataresult["actions"] = { "Watch": videoLink, "Learn": videoLink, "Read and Repeat": videoLink, "Listen and Repeat": videoLink, "Listen and Write": videoLink };


    // EMPTY CHART
    dataresult["chartdata"] = {};
    dataresult["chartext"] = "";
    dataresult["chartsubtext"] = "";

    return dataresult;
}
function Course_Class_ActivityTest(data, state, source) {
    var dataresult = {};

    // COVER IMG
    dataresult["img"] = "content/lessons/" + data["lesson"]["source"] + "/cover.png";
    if (fs.existsSync('./../lab/' + dataresult["img"])) { } else dataresult["img"] = "images/cover-test.jpg";

    // COVER CAPTION
    dataresult["caption"] = "HOMEWORK";

    // ACTIONS
    dataresult["actions"] = { 'Pre Lesson Assessment': null, 'Free Practice': null, 'After Lesson Improvement Test': null };

    let results = [];
    data["activities"].forEach(element => {
        if (element["source"].search("test") >= 0) results.push(element);
    })
    // CHART
    var chart = Course_Class_RowChart(results, state);
    dataresult["chartdata"] = chart;
    // CHART TEXT
    var chartext = Course_Class_RowChartText(results, state)
    dataresult["chartext"] = chartext[0];
    dataresult["chartsubtext"] = chartext[1];

    return dataresult;
}
function Course_Class_ActivityVocabulary(data, state) {
    var dataresult = {};
    // COVER IMG
    dataresult["img"] = "content/vocabulary/" + (Object.values(data["lesson"]["vocabulary"] || {})[0] || '') + "/picture.png";
    if (fs.existsSync('./../lab/' + dataresult["img"])) {
        dataresult["img"] = "content/vocabulary/" + rawurlencode(Object.values(data["lesson"]["vocabulary"] || {})[0] || '') + "/picture.png";
     } else dataresult["img"] = "images/cover-dictionary.jpg";

    // COVER CAPTION
    dataresult["caption"] = "VOCABULARY";

    // ACTIONS
    dataresult["actions"] = { 'Pre Lesson Assessment': null, 'Free Practice': null, 'After Lesson Improvement Test': null };

    let results = [];
    data["activities"].forEach(element => {
        if (element["source"].search("vocabulary") >= 0) results.push(element);
    })
    // CHART
    var chart = Course_Class_RowChart(results, state);
    dataresult["chartdata"] = chart;
    // CHART TEXT
    var chartext = Course_Class_RowChartText(results, state)
    dataresult["chartext"] = chartext[0];
    dataresult["chartsubtext"] = chartext[1];

    return dataresult;
}
function findFileName(lesson, file) {
    for (let key in lesson) {
        let element = lesson[key];
        if (element["file"] == file && typeof element["en"] != "undefined") return element["en"];
    }
    return file.slice(0, -4);
}
function Course_Class_ActivityDocuments(data, state, source) {
    var dataresult = {};
    // COVER IMG
    dataresult["img"] = "resources/images/cover-presentation.jpg";

    // COVER CAPTION
    dataresult["caption"] = "MATERIALS";

    // ACTIONS
    dataresult["actions"] = {};
    for (var id in data["lesson"]["documents"]) {
        var file = data["lesson"]["documents"][id];
        var text = findFileName(data["lesson"], file);

        dataresult["actions"][text] = "content/lessons/" + data["lesson"]["source"] + "/documents/" + rawurlencode(file);
    }

    // EMPTY CHART
    dataresult["chartdata"] = {};
    dataresult["chartext"] = "";
    dataresult["chartsubtext"] = "";

    return dataresult;
}
function Date_From_JS(date) {
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    return String(year) + String(month).padStart(2, "0") + String(day).padStart(2, "0") + String(hour).padStart(2, "0") + String(minutes).padStart(2, "0");
}

module.exports = (data) => {
    // ADD SOME MANDATORY CATEGORIES 
    var folders = [...data["content"]];
    if (!data["content"].includes("vocabulary")) folders.push("vocabulary");
    // SET STATE
    var state = '';
    now = new Date();
    if (Date_From_JS(now) > data['class']['date_end']) state = 'post'; else state = 'pre';
    var content = {};
    for (category of ["video", "test", "vocabulary", "documents"]) {
        for (var folder of folders) if (folder.includes(category)) {
            switch (category) {
                case "test":
                    caption = "homework";
                    content[caption] = Course_Class_ActivityTest(data, state, folder);
                    break;
                case "documents":
                    caption = "materials";
                    content[caption] = Course_Class_ActivityDocuments(data, state, folder);
                    break;
                case "video":
                    caption = "video";
                    content[caption] = Course_Class_ActivityVideo(data, state, folder);
                    break;
                case "vocabulary":
                    caption = "vocabulary";
                    content[caption] = Course_Class_ActivityVocabulary(data, state, folder);
                    break;
            }
        }
    }
    return content;
}