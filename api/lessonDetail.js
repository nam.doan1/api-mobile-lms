var conn = require('../connection');
var ini = require('ini');
var fs = require('fs');
var path = require('path');
var getContent = require('./lessonContent');
var getAssessment = require('./lessonAssessment');

const getDetailLesson = async (req, res) => {
    var seat_id = req.body.seat_id;
    // GET BASIC SEAT INFORMATION
    var seat = await Class_Seat_Read(seat_id, { users: true });
    // USE CLASS ID TO READ LESSON ID
    var class_id = seat["class_id"];
    var classes = await Class_Read(class_id, { teacher: true });
    // READ LESSON WITH OUTCOMES
    var lesson_id = classes["info"]["lesson_id"];
    var lesson = await Lesson_Read(lesson_id, { outcomes: true });
    // READ STORED ACTIVITIES RESULTS TIED TO THIS STUDENT AND LESSON
    var student_id = seat["student_id"];
    var activities = await Activities_Results_ReadBySource(student_id, lesson_id, false);
    // PARSE LESSON FOLDER TO SEE WHAT CONTENT SUBFOLDERS ARE AVAILABLE WITHIN
    var content = Storage_Folder_ListFolders("./../lab/content/lessons/" + lesson_id);

    data = {}
    data["seat"] = seat;
    data["class"] = classes["info"];
    data["teacher"] = classes["teacher"];
    data["lesson"] = lesson;
    data["content"] = content;
    data["activities"] = activities;

    detail = {};

    detail["content"] = getContent(data);
    detail["assessment"] = getAssessment(data)
    res.json(detail);
}
////////////////////////////////////////////////////////////////////////////////////Class_Seat_Read//////////////////////////////////////////////////////////////////////////////
async function getClassSeat(id) {
    return new Promise((resolve, reject) => {
        let query = "SELECT * FROM classes_seats WHERE id = " + id;
        conn.query(query, function (error, results, fields) {
            resolve(results[0]);
        });
    })
}
async function getUserName(id) {
    return new Promise((resolve, reject) => {
        let query = "SELECT id, firstname, lastname FROM users WHERE id = " + id;
        conn.query(query, function (error, results, fields) {
            resolve(results[0]);
        });
    })
}
async function Class_Seat_Read(id, options) {

    var seat = await getClassSeat(id);
    seat["assessment"] =(!seat['assessment'])?{}: JSON.parse(seat["assessment"]);
    seat["badges"] = (!seat["badges"])?{}:JSON.parse(seat["badges"]);
    if (options.users || options.all) {
        student_id = seat["student_id"];
        // RETRIEVE INFO
        seat["student"] = await getUserName(student_id);
    }
    return seat;
}

////////////////////////////////////////////////////////////////////////////////////////////Class_Read///////////////////////////////////////////////////////////////////////////
function array_column(array, column) {
    var newarray = [];
    array.forEach(element => {
        newarray.push(element[column]);
    });
    return newarray;
}
function SQL_Values_List(array) {
    var data = '(';
    array.forEach(element => {
        data = data + element + ',';
    });
    data = data.slice(0, data.length - 1) + ')';
    return data;
}
async function Class_Read(class_id, options) {
    var classes = [];

    // READ CLASS
    let rows = await new Promise(function (resolve, reject) {
        let query = "SELECT * FROM classes WHERE id = " + class_id;
        conn.query(query, function (error, results, fields) {
            resolve(results);
        });
    });
    if (Object.keys(rows).length === 0) {
        return [];
    }
    classes["info"] = rows[0];
    // RETRIEVE SEATS
    if (options["seats"] || options["all"]) {
        let seats = await new Promise((resolve, reject) => {
            conn.query(query, function (error, results, fields) {
                resolve(results);
            });
        });
        classes["seats"] = seats;

        // IF USERS REQUESTED, INTEGRATE EACH SEAT WITH ITS STUDENT'S INFO
        if (options["users"] || options["students"] || options["all"]) {
            ids = array_column(results, "student_id");
            if (ids.length > 0) {
                ids = SQL_Values_List(ids);

                let students = await new Promise((resolve, reject) => {
                    let query = "SELECT id, firstname, lastname, role FROM users WHERE id IN (" + ids + ")";
                    conn.query(query, function (err, results, fields) {
                        resolve(results);
                    });
                });

                classes["seats"].forEach(element => {
                    let search = element["student_id"];
                    students.every(student => {
                        if (student["id"] == search) { element.student = student; return false; }
                        return true;
                    });
                });
            }
        }
        classes["seats"] = seats;
    }
    // IF USERS REQUESTED, ADD TEACHER INFO
    if (options["users"] || options["teacher"] || options["all"]) {
        id = classes["info"]["teacher_id"];

        // RETRIEVE INFO
        classes["teacher"] = await new Promise((resolve, reject) => {
            let query = "SELECT id, firstname, lastname, role FROM users WHERE id = " + id;
            conn.query(query, function (err, results, fields) {
                resolve(results[0]);
            });
        });
    }
    return classes;
}

//////////////////////////////////////////////////////////////////////////////////////Lesson_Read///////////////////////////////////////////////////////////////////////////
function Outcome_Read(folder) {
    return ini.parse(fs.readFileSync(folder + '/info.dat', 'utf-8'));
}

function Lesson_Assessables(lesson, info = false) {
    var assessables = [];

    if (typeof lesson == "string") var data = ini.parse(fs.readFileSync('./../lab/content/lessons/' + source + '/info.dat', 'utf-8'));
    else data = lesson;
    curriculum = data["info"]["curriculum"] || "default";


    // LESSON-SPECIFIC OUTCOMES
    assessables["outcomes"] = data["outcomes"] ?? [];

    // SKILLS FOR THE CURRICULUM THE LESSON BELONGS TO
    skills = ini.parse(fs.readFileSync('./../lab/content/curricula/' + curriculum + '/info.dat', 'utf-8'));
    assessables["core skills"] = Object.keys($skills["core skills"]) || [];

    // COMPLEMENT WITH DETAILED INFO IF REQUESTED
    keys = Object.keys(assessables["core skills"]);

    keys.forEach(element => {
        assessables["core skills"][$key] = Outcome_Read("./../lab/content/skills/" + key)
    });

    return assessables;
}
function Vocabulary_Read_Term(source) {
    term = ini.parse(fs.readFileSync(source + '/info.dat', 'utf-8'));
    term["source"] = source;

    // DOES IT HAVE SAMPLED AUDIO?
    audio = source + "/audio.mp3";
    if (fs.existsSync(audio)) {
        term["audio"] = audio;
    }

    return term;
}
function Storage_Files_Collect(root, types, options = []) {
    var folders = [];
    var files = []

    folders[0] = root;

    recurse = options.includes("recurse");
    uproot = options.includes("uproot");

    while (folders.length > 0) {
        let folder = folders[0];
        folders.shift();

        data = fs.readdirSync(folder);
        data.forEach(item => {
            if ((item != '.') && (item != '..')) {
                let file = folder + '/' + item;
                if (fs.lstatSync(file).isDirectory()) {
                    if (recurse) folders.push(file);
                } else {
                    ext = path.extname(file);
                    if (!types || types.includes(ext)) {
                        let id;
                        if (uproot) id = file.replace(root, "");
                        else id = file;
                        files.push(id);
                    }
                }
            }
        });
    }

    return files;
}
async function Lesson_Read(source, data = false) {

    if(fs.existsSync('./../lab/content/lessons/' + source + '/info.dat'))
    lesson = ini.parse(fs.readFileSync('./../lab/content/lessons/' + source + '/info.dat', 'utf-8'));
    else lesson = {};
    // SPECIAL CASES. USED BECAUSE RETURNED DATA FOR MULTIPLE LESSONS COULD BE A LOT
    switch (data) {
        case "lesson-only":
            return lesson;
            break;

        case "title-only":
            return lesson["title"];
            break;

        case "base-info":
            let info = [];
            info["title"] = lesson["title"];
            info["assessables"] = Lesson_Assessables(lesson);

            return info;
            break;
    }


    // STANDARD READ

    lesson["source"] = source;

    // READ ASSESSABLE ITEMS
    if (data["assessables"]) {
        lesson["assessables"] = Lesson_Assessables(lesson);
    }


    // READ OUTCOMES AND SKILLS
    if (data["outcomes"] || data["all"]) {
        // DETERMINE SKILLS FROM LESSON'S CURRICULUM
        var curriculum;
        if (typeof data["info"] == "undefined") curriculum = "default";
        else {
            if (typeof data["info"]["curriculum"] == "undefined") curriculum = "default";
            else curriculum = data["info"]["curriculum"];
        }
        if(fs.existsSync('./../lab/content/curricula/' + curriculum + '/info.dat'))
        skills = ini.parse(fs.readFileSync('./../lab/content/curricula/' + curriculum + '/info.dat', 'utf-8'))
        else skills = {};
        // READ SKILLS
        keys = Object.keys(skills["core skills"] ||{}) || [];
        lesson["core skills"] = {};
        keys.forEach(key => {
            lesson["core skills"][key] = Outcome_Read("./../lab/content/skills/" + key);
        });

        // READ OUTCOMES
        let idoutcomes = Object.values(lesson["outcomes"] || {}) || [];
        lesson["outcomes"] = {};
        idoutcomes.forEach(id => {
            lesson["outcomes"][id] = Outcome_Read("./../lab/content/outcomes/" + id);
        });
    }


    // LOAD VOCABULARY
    if (data && data["all"] || data["vocabulary"]) {
        let ids = [...lesson["vocabulary"]];
        lesson["vocabulary"] = [];
        ids.forEach(id => {
            outcome = Vocabulary_Read_Term("./../content/vocabulary/" + id);
            lesson["vocabulary"][id] = outcome;
        });
    }


    // DOCUMENTS FOLDER
    if (data && data["all"] || data["documents"]) {
        files = Storage_Files_Collect("./../lab/content/lessons/" + source + "/documents", ["pdf"], ["uproot"]);

        lesson["documents"] = files;
    }

    return lesson;
}
//////////////////////////////////////////////////////////////////////////////////Activities_Results_ReadBySource///////////////////////////////////////////////////////////////////
async function Activities_Results_ReadBySource(student_id, source, strict = true) {

    if (strict) {
        condition = "(source = \"" + source + "\")";
    }
    else {
        condition = "(source LIKE \"" + source + "%\")";
    }

    let query = "SELECT * FROM users_activities WHERE (student_id = " + student_id + ") AND " + condition + " ORDER BY source, date";
    var rows = await new Promise(function (resolve, reject) {
        conn.query(query, function (error, results, fields) {
            resolve(results);
        });
    })
    rows.forEach(item => {
        let keys = Object.keys(item);
        keys.forEach(key => {
            if (["data"].includes(key)) {
                item[key] = JSON.parse(item[key]);
            }
        });
    });
    return rows;
}
///////////////////////////////////////////////////////////////////////////Storage_Folder_ListFolders/////////////////////////////////////////////////////////
function Storage_Folder_ListFolders(folder) {
    if(fs.existsSync(folder))
    var files = fs.readdirSync(folder);
    else return [];
    var list = [];
    files.forEach(file => {
        if (file != "." && file != ".." && fs.lstatSync(folder + '/' + file).isDirectory()) list.push(file);
    });

    return list;
}

module.exports = {
    getDetailLesson
}