const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const { signinHandler, welcomeHandler, refreshHandler ,logoutHandler,authMiddleware} = require('./handlers')
const {getDetailLesson} = require('./api/lessonDetail');
const {getProgress}  = require('./api/progress');
const {homeStudentData}  = require('./api/homeStudentData');
const app = express()
app.use(bodyParser.json())
app.use(cookieParser())

app.post('/signin', signinHandler);
app.get('/welcome', welcomeHandler);
app.post('/refresh', refreshHandler);
app.get('/logout', logoutHandler);
// app.use('/detail', authMiddleware);
app.get('/detail', getDetailLesson);
app.get('/progress',getProgress);
app.get('/Home_Student_Data',homeStudentData);
app.listen(8080);