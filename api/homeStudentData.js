var conn = require('../connection');
var fs = require('fs');
var ini = require('ini');
function Date_Now() {
    let today = new Date();
    let yyyy = today.getFullYear();
    let mm = today.getMonth() + 1; // Months start at 0!
    let dd = today.getDate();
    let hh = today.getHours();
    let ii = today.getMinutes();
    if (dd < 10) dd = '0' + dd;
    if (mm < 10) mm = '0' + mm;
    if (hh < 10) hh = '0' + hh;
    if (ii < 10) ii = '0' + ii;
    return yyyy + '' + mm + '' + dd + '' + hh + '' + ii;
}
async function Class_Seats_ListByStudent(student_id = -1, date_from = "190001010000", date_to = "290001010000", options = false) {
    if (student_id == -1) return {};

    var order = "ORDER BY classes.date_start";
    var fields = ["classes.date_start", "classes.teacher_id", "classes.lesson_id", "classes.duration", "classes_seats.id", "classes_seats.attendance"];
    var limit = "";

    // GET LAST ONES?
    if (typeof options["last"] != "undefined") {
        order = "ORDER BY -classes.date_start";
        limit = "LIMIT 0, 1";
    }

    // LIMIT AMOUNT?
    if (typeof options["limit"] != "undefined") {
        limit = "LIMIT 0, " + options["limit"];
    }

    // ALSO EXTRACT ASSESSMENT?
    if (typeof options["assessment"] != "undefined") {
        fields.push("classes_seats.assessment")
    }

    // GET SEATS
    fields = fields.join(", ");
    let query = "SELECT " + fields
        + " FROM classes, classes_seats"
        + " WHERE(classes.date_start BETWEEN'" + date_from + "' AND'" + date_to + "') AND(classes.id = classes_seats.class_id) AND(classes_seats.student_id =" + student_id + ")"
        + order + ' ' + limit;


    var seats = await new Promise(resolve => {
        conn.query(query, function (err, results, fields) {
            resolve(results);
        })
    })
    // EXTRA INFO: LESSON HEADER
    if (typeof options["lesson"] != "undefined") {
        for (let key in seats) {
            let seat = seats[key];
            let lesson_id = seat["lesson_id"];
            let info = {'info':null,'title':null};
            if (fs.existsSync("./../lab/content/lessons/" + lesson_id + "/info.dat")) { 
                let data = ini.parse(fs.readFileSync("./../lab/content/lessons/" + lesson_id + "/info.dat", "utf-8"));
                ["info", "title"].forEach(section => {
                    info[section] = data[section];
                });
            }
            seat["lesson"] = info;
        }
    }
    return seats;
}
async function homeStudentData(req, res) {
    var student_id = req.body.student_id;
    if (typeof student_id == "undefined") return {};
    var data = {};


    // GET LAST CLASS TAKEN
    today = Date_Now();
    classes = await Class_Seats_ListByStudent(student_id, "190001010000", today, { "last": true, "lesson": true, "limit": 3 });
    data["last-class"] = classes;

    // GET NEXT CLASS TO ATTEND
    classes = await Class_Seats_ListByStudent(student_id, today, "290001010000", { "lesson": true });
    data["next-class"] = classes[0] || [];

    res.send(data);
}
module.exports = { homeStudentData }